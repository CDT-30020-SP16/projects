''' searchit.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'searchit.db'
    YAML_PATH   = 'assets/yaml/data.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        # TODO: Set instance variables and call _create_tables and _load_tables
        self.logger = logging.getLogger()
        self.data   = None
        self.path   = None
        self.conn   = None

    def _create_tables(self):
        # TODO: Create Artist, Album, and Track tables
        pass

    def _load_tables(self):
        # TODO: Insert Artist, Album, and Track tables from YAML
        pass

    def artists(self, artist):
        # TODO: Select artists matching query
        pass

    def artist(self, artist_id=None):
        # TODO: Select artist albums
        pass

    def albums(self, album):
        # TODO: Select albums matching query
        pass

    def album(self, album_id=None):
        # TODO: Select specific album
        pass

    def tracks(self, track):
        # TODO: Select tracks matching query
        pass

    def track(self, track_id=None):
        # TODO: Select specific track
        pass

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
