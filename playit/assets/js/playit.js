/* Globals */

var PlaylistQueue   = [];	    /* List of songs	*/
var PlaylistIndex   = -1;	    /* Current song	*/
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */

    /* TODO: Add active class to current object */

    /* TODO: Check current object's id and call the appropriate display function */
}

function displayResults(url) {
    /* TODO: Request JSON from URL and then call appropriate render function */
}

function displaySearch(query) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to display search form */

    /* TODO: Call updateSearchResults */
}

function displayArtist(artist) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: If artist is undefined, then set to empty string */

    /* TODO: Call displayResults for appropriate /artist/ URL */
}

function displayAlbum(album) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: If album is undefined, then set to empty string */

    /* TODO: Call displayResults for appropriate /album/ URL */
}

function displayTrack(track) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: If album is undefined, then set to empty string */

    /* TODO: Call displayResults for appropriate /album/ URL */
}

function displayPlaylist() {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */

    /* TODO: Set notebook-body-contents to playlist table
     *
     *	- Table should have playlist identifier.
     *	- Table should have columns for playlist index, album cover, artist, album, and track names.
     *	- Table should highlight the active song.
     *  - Table should be generated by looping through objects in PlaylistQueue.
     *
     *  - Optional: add callback so that if user clicks on song, then the song is played.
     */
}

/* Render functions */

function renderGallery(data, prefix) {
    /* TODO: Set notebook-body-contents to gallery of thumbnails
     *
     *	- Similar to gallery.html template
     *	- Each item should have an anchor in like this:
     *
     *	    <a href="#" onclick="displayPREFIX(ID)" class="thumbnail text-center">

     *	  Where PREFIX is the prefix given and the ID is the id from the
     *	  current data item.  For instance if PREFIX is Track and id is 1, then
     *	  the anchor would be:
     *
     *	    <a href="#" onclick="displayTrack(1)" class="thumbnail text-center">
     */
}

function renderAlbum(data) {
    /* TODO: Set notebook-body-contents to table of tracks
     *
     *	- Each track should have an icon anchor for adding the song to playlist
     *	or for playing the song.
     *
     */
}

function renderTrack(data) {
    /* TODO: Set notebook-body-contents to display of track information
     *
     *	- Clicking on the artist name should call displayArtist
     *	- Clicking on the album name should call displayAlbum
     *	- There should be icons for adding song to playlist or for playing the song.
     *
     */
}

/* Update functions */

function updateSearchResults() {
    /* TODO: Construct search URL from query and table values and call
     * displayResults
     */
}

function updateSongInformation() {
    /* TODO: Get current song from PlaylistQueue and update the following elements:
     *
     *	1. trackName.innerHTML
     *	2. albumImage.innerHTML
     *	3. artistName.innerHTML
     *	4. image.src
     *	5. player.src
     *
     *	If CurrentNotebook is Playlist, then call displayPlaylist.
     */
}

/* Audio controls */

function addSong(trackId, select) {
    var url = "/song/" + trackId;

    requestJSON(url, function(data) {
	var song = data['song'];
	PlaylistQueue.push(song);

	if (select !== undefined && select) {
	    selectSong(PlaylistIndex + 1);
	}
    });
}

function prevSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + PlaylistQueue.length - 1) % PlaylistQueue.length);
}

function nextSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + 1) % PlaylistQueue.length);
}

function selectSong(index) {
    PlaylistIndex = index;
    updateSongInformation();
    playSong();
}

function playSong(trackId) {
    var player     = document.getElementById('player');
    var playButton = document.getElementById('playButton');

    if (trackId !== undefined && trackId != '') {
    	return addSong(trackId, true);
    }

    if (PlaylistIndex < 0 && PlaylistQueue.length) {
    	return selectSong(0);
    }

    if (player.src === undefined || player.src == '') {
    	return;
    }

    if (player.ended || player.paused) {
    	playButton.innerHTML = '<i class="fa fa-pause"></i>';
	player.play();
    } else {
    	playButton.innerHTML = '<i class="fa fa-play"></i>';
	player.pause();
    }
}

function togglePlaySong() {
    playSong('');
}

function endSong() {
    document.getElementById('playButton').innerHTML = '<i class="fa fa-play"></i>';
    nextSong();
}

/* Registrations */
window.onload = function() {
    /* TODO: Register displayTab function for each tab's onclick callback */

    /* TODO: Register onclick callbacks for
     *
     *	- prevButton -> prevSong
     *	- playButton -> togglePlaySong
     *	- nextButton -> nextSong
     */

    /* TODO: Register endSong function for player's onended callback */

    /* TODO: Display search results */
}
