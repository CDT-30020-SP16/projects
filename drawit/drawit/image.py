''' image.py: DrawIt Image '''

from color import Color
from point import Point

import math

class Image(object):
    DEFAULT_WIDTH  = 640
    DEFAULT_HEIGHT = 480

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ''' Initialize Image object's instance variables '''
        # TODO

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        # TODO

    def __str__(self):
        ''' Returns string representing Image object '''
        # TODO

    def __getitem__(self, point):
        ''' Returns pixel specified by point

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        # TODO

    def __setitem__(self, point, color):
        ''' Sets pixel specified by point to given color

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        # TODO

    def clear(self):
        ''' Clears Image by setting all pixels to default Color '''
        # TODO

    def draw_line(self, point0, point1, color):
        ''' Draw a line from point0 to point1 '''
        # TODO

    def draw_rectangle(self, point0, point1, color):
        ''' Draw a rectangle from point0 to point1 '''
        # TODO

    def draw_circle(self, center, radius, color):
        ''' Draw a circle with center point and radius '''
        # TODO

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
