''' color.py: DrawIt Color '''

class Color(object):
    def __init__(self, r=0, g=0, b=0):
        ''' Initialize Color object's instance variables '''
        # TODO

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        # TODO

    def __str__(self):
        ''' Returns string representing Color object '''
        # TODO

# TODO: Define basic colors
WHITE   = Color()
RED     = Color()
GREEN   = Color()
BLUE    = Color()
CYAN    = Color()
MAGENTA = Color()
YELLOW  = Color()
BLACK   = Color()

# TODO: Define list of basic colors
COLORS  = [
    WHITE,
]

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
