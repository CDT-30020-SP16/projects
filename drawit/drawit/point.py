''' point.py: DrawIt Point '''

import math

class Point(object):

    def __init__(self, x=0, y=0):
        ''' Initialize Point object's instance variables '''
        # TODO:

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        # TODO:

    def __str__(self):
        ''' Returns string representing Point object '''
        # TODO:

    def distance_from(self, other):
        ''' Returns distance from current instance to other Point '''
        # TODO:

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
